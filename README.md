# README #
This is data acquisition software for Mechatron SA trackers. 
The data will be used for IEC reporting.
The software obtains data from a Rabbit micro-processor with a central ISS-D5 sensor.
3 additional ISS-d5 sensors are located on 3 corners of the C-140 experimental tracker


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

     * Make sure you have .NET 4.0 up to date
     * Download Advantech EKI 1524 Serial Server
     * Download the binary  or source and compile
     * run the software

* Configuration

     * EKI ports need to have the following configuration
          * COM11 : Tracker
          * COM12 : ISS Sensor
          * COM13 : ISS Sensor
          * COM14 : ISS Sensor

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
npapnet AT gmail.com
* Other community or team contact