﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using npap.cs.cpv;

namespace UnitTestCPVCommunication
{
    /// <summary>
    /// Summary description for utCpvDataParse
    /// </summary>
    [TestClass]
    public class utCpvDataParser
    {
        public utCpvDataParser()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CpvParserTestMethod1()
        {
            //
            // TODO: Add test logic here
            //
            string response = "!,48,48,48,49,100,229,255,37,65,3,1,166,5,57,67,25,91,127,66,179,0,0,0,28,0,0,0,1,0,0,0,0,0,0,0,0,0,0,16,65,71,225,170,65,0,0,0,195,255,37,65,56,121";
            DateTime dtCurrent = new DateTime(2014, 8, 29, 13, 42, 1);
            DateTime dtNextTracking= new DateTime(2014,8,29,13,42,27 );
            CpvDataParser cpvParser = new CpvDataParser();
            CpvData cpvData = cpvParser.ParseData(response);

            Assert.AreEqual(0.0, (dtCurrent - cpvData.CPVTimestamp).Seconds, 1); 
            Assert.AreEqual(1, cpvData.TrackerID);
            Assert.AreEqual(3, cpvData.TimeZone);
            Assert.AreEqual(1, cpvData.TrackingMode);
            Assert.AreEqual(185, cpvData.AstronomicalPosition.X, 1.0);
            Assert.AreEqual(63.8, cpvData.AstronomicalPosition.Y, 1.0);
            Assert.AreEqual(179, cpvData.EncoderPosition.X );
            Assert.AreEqual(28, cpvData.EncoderPosition.Y);
            Assert.AreEqual(1, cpvData.StatusID);
            Assert.AreEqual(0, cpvData.SunDeviationISS.X, 0.5);
            Assert.AreEqual(0, cpvData.SunDeviationISS.Y, 0.5);
            Assert.AreEqual(9.0, cpvData.ISSRadiation, 1);
            Assert.AreEqual(21.36, cpvData.WindSpeed, 0.5);
            Assert.AreEqual(0, cpvData.ErrorCode);
            Assert.AreEqual(0, cpvData.Inputs1);
            Assert.AreEqual(0, cpvData.Inputs2);
            Assert.AreEqual(0.0, (dtNextTracking- cpvData.NextTrackingTime).Seconds,1); 
        }
    }
}
