﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using npap.cs.iss;

namespace UnitTestCPVCommunication
{
    [TestClass]
    public class utISSSensor
    {
        /// <summary>
        /// Event which handles saving to file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [TestMethod]
        public void TestPerformMeasurement()
        {
            ISSComms isscomms = new ISSComms("COM12");
            ISSData issData ;
            isscomms.InitSerialPort();
            isscomms.SendRequest();
            Thread.Sleep(200);
            isscomms.ReadFromPort();
            //System.Windows.Forms.MessageBox.Show(isscomms.DataAsHexString);
            issData = isscomms.issData;
            Assert.AreEqual(16, isscomms.DataAtSerialPort);
            Assert.AreEqual(true, issData.Accepted);

        }
    }
}
