﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using npap.cs.cpv;

namespace UnitTestCPVCommunication
{
    [TestClass]
    public class utCpvData
    {
        #region Global Variables
        //SerialPort TPort = new SerialPort("COM12",9600, Parity.None,8, StopBits.Two);  // Tracker port
        SerialPort TrackerPort = new SerialPort("COM11", 9600, Parity.None, 8, StopBits.One);  // Tracker port
        SerialPort IssPort = new SerialPort("COM12", 115200, Parity.None, 8, StopBits.One);  // ISSport;
        CpvDataParser cdp = new CpvDataParser();
        int discardedDataCounter = 0;           // Discarded Data Counter
                 
        #endregion

        [TestMethod]
        public void testComms()
        {
            CpvComms cpvComms = new CpvComms(TrackerPort);
            string strout = cpvComms.TrackerPortGetData();
            Assert.AreNotEqual(0, strout.Length);
            Assert.AreEqual(cpvComms.testDataParallelism, strout);
        }

        [TestMethod]
        public void testCommsAndParsing()
        {
            CpvComms cpvComms = new CpvComms(TrackerPort);
            string strout = cpvComms.TrackerPortGetData();
            CpvData cd = cdp.ParseData(strout);
            Assert.AreNotEqual(0, strout.Length);
            Assert.AreEqual(cpvComms.testDataParallelism, strout);
            Assert.AreNotEqual(255, cd.ErrorCode);
            Assert.AreNotEqual(255, cd.Inputs1);
            Assert.AreNotEqual(255, cd.Inputs2);
            Assert.AreNotEqual(CpvDataParser.dtReferenceOrigin, cd.NextTrackingTime);
        }


        [TestMethod]
        public void TestCommsParallelExecution()
        {
            //Parallel.Invoke(() => TrackerPortGetData(this.TrackerPort), () => DoSomeOtherWork());
        }
        #region Auxilliary Functions
        [TestMethod]
        public void TestAsciiEncoding()
        {
//System.Windows.Forms.MessageBox.Show(DateTime.Now.ToString("yyyyMMddHHmmss"));

            Assert.AreEqual("001", System.Text.ASCIIEncoding.ASCII.GetString(new byte[] { 48, 48, 49 }));
        }

        private object DoSomeOtherWork()
        {
            throw new NotImplementedException();
        }

               
        void TrackerPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
             discardedDataCounter++;
        }


        #endregion

 
    }
}
