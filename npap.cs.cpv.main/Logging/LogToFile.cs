﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace npap.cs.cpv.main.Logging
{
    public class LogToFile
    {
        public bool IsLoggerActive = false;           // Save to file. 
        public string BaseDir { get; set; }
        System.IO.StreamWriter file = null;   // File 
        public DateTime dtLastAccess;
        public string HeaderString { get; set; }

        public LogToFile(string BaseDirectory, string HeadersString)
        {
            BaseDir = BaseDirectory;
            dtLastAccess = DateTime.Now;
            this.HeaderString = HeadersString;
            InitialiseNewFileStream();
        }

        /// <summary>
        /// Event which handles saving to file.
        /// </summary>
        /// <param name="sender"></param>
        public void SaveToFile(string str)
        {
            // check if Initialise New FileStream needs to be invoked
            if (file == null || DateTime.Now.Day != dtLastAccess.Day)
            {
                InitialiseNewFileStream();
            }
            try
            {
                if (IsLoggerActive && file.BaseStream != null)
                {
                    //string str = Xcnt.ToString() + "\t" + String.Format("{0:yyyy/MM/dd}\t{1:HH:mm:ss.ff}", DateTime.Now, DateTime.Now) + "\t" + issControl.getString();//"" + k.ToString() + "" + (k * k).ToString();
                    file.WriteLine(str);
                    Console.WriteLine(str);
                    dtLastAccess = DateTime.Now;
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Save To file button event. 
        /// this event sets the save to file flag
        /// and also initialises the file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void InitialiseNewFileStream()
        {
            try
            {
                file.Close();
                IsLoggerActive = false;
            }
            catch (NullReferenceException)
            { }
            string filename = string.Format("{0}cpv{1}.txt", BaseDir, DateTime.Now.ToString("yyyyMMdd-HHmm"));
            //System.Windows.Forms.MessageBox.Show(filename);
            file = new System.IO.StreamWriter(filename, true);
            file.AutoFlush = true;
            file.WriteLine(HeaderString);
            IsLoggerActive = true;
        }




    }
}
