﻿namespace npap.cs.cpv.main
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbDebug = new System.Windows.Forms.TextBox();
            this.btnStartStopDaqProcess = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbDebug
            // 
            this.tbDebug.Location = new System.Drawing.Point(13, 13);
            this.tbDebug.Multiline = true;
            this.tbDebug.Name = "tbDebug";
            this.tbDebug.ReadOnly = true;
            this.tbDebug.Size = new System.Drawing.Size(403, 134);
            this.tbDebug.TabIndex = 0;
            this.tbDebug.TextChanged += new System.EventHandler(this.tbDebug_TextChanged);
            // 
            // btnStartStopDaqProcess
            // 
            this.btnStartStopDaqProcess.Location = new System.Drawing.Point(32, 295);
            this.btnStartStopDaqProcess.Name = "btnStartStopDaqProcess";
            this.btnStartStopDaqProcess.Size = new System.Drawing.Size(155, 23);
            this.btnStartStopDaqProcess.TabIndex = 1;
            this.btnStartStopDaqProcess.Text = "Start Daq Process";
            this.btnStartStopDaqProcess.UseVisualStyleBackColor = true;
            this.btnStartStopDaqProcess.Click += new System.EventHandler(this.btnStartStopDaqProcess_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 379);
            this.Controls.Add(this.btnStartStopDaqProcess);
            this.Controls.Add(this.tbDebug);
            this.Name = "frmMain";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbDebug;
        private System.Windows.Forms.Button btnStartStopDaqProcess;
    }
}

