﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace npap.cs.cpv.main
{
    public partial class frmMain : Form
    {

        #region Variable Declaration
        #region Configuation 

        Logging.LogToFile lgr ;
        private const int timerInterval = 1000;// [ms]
        #endregion

        Sensors.DAQSystem daqSystem = new Sensors.DAQSystem();
        System.Windows.Forms.Timer tmr = new Timer();
        StringBuilder sb = new StringBuilder();
        #endregion

        public frmMain()
        {
            InitializeComponent();
            tmr.Tick += tmr_Tick;
            tmr.Interval = timerInterval;

            lgr = new Logging.LogToFile("c:\\cpv\\", prepareHeaderString());
        }

        private string prepareHeaderString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(daqSystem.cpv.Headers(0));
            sb.Append(daqSystem.iss1.Headers(1));
            sb.Append(daqSystem.iss2.Headers(2));
            sb.Append(daqSystem.iss3.Headers(3));
            return sb.ToString();
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            tbDebug.Text = sb.ToString();
            Task daqFirstTask = new Task(() => daqSystem.PerformMeasurement());
            Task daqSecondTask = daqFirstTask.ContinueWith((t) =>
            {
                sb.Clear();
                sb.AppendFormat(DateTime.Now.ToLongTimeString());
                sb.AppendFormat(Environment.NewLine);
                sb.AppendFormat("CPV| \tX:{0}, \tY:{1}, \tRad:{2}{3}", daqSystem.cpv.CPVData.SunDeviationISS.X.ToString("0.000"), daqSystem.cpv.CPVData.SunDeviationISS.Y.ToString("0.000"), daqSystem.cpv.CPVData.ISSRadiation, Environment.NewLine);
                sb.AppendFormat("iss1| \tX:{0}, \tY:{1}, \tRad:{2}{3}", daqSystem.iss1.issData.X.ToString("0.000"), daqSystem.iss1.issData.Y.ToString("0.000"), daqSystem.iss1.issData.Radiation, Environment.NewLine);
                sb.AppendFormat("iss2| \tX:{0}, \tY:{1}, \tRad:{2}{3}", daqSystem.iss2.issData.X.ToString("0.000"), daqSystem.iss2.issData.Y.ToString("0.000"), daqSystem.iss2.issData.Radiation, Environment.NewLine);
                sb.AppendFormat("iss3| \tX:{0}, \tY:{1}, \tRad:{2}{3}", daqSystem.iss3.issData.X.ToString("0.000"), daqSystem.iss3.issData.Y.ToString("0.000"), daqSystem.iss3.issData.Radiation, Environment.NewLine);

            });
            Task daqTask3 = daqFirstTask.ContinueWith((t) =>
            {
                StringBuilder sb1 = new StringBuilder();
                sb1.Append( daqSystem.cpv.DataString());
                sb1.Append(daqSystem.iss1.DataString());
                sb1.Append(daqSystem.iss2.DataString());
                sb1.Append(daqSystem.iss3.DataString());
                //tbDebug.BeginInvoke(new Action(()=>{tbDebug.Text=str;}));
                Console.WriteLine(sb1.ToString());
                lgr.SaveToFile(sb1.ToString());
            });
            daqFirstTask.Start();        
            
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnStartStopDaqProcess_Click(object sender, EventArgs e)
        {
            if (daqSystem.isActive)
            { // Operations when closing 
                btnStartStopDaqProcess.Text = "Start DAQ Process";
                daqSystem.ClosePorts();
                tmr.Stop();
            }
            else
            { // Operations when opening 
                btnStartStopDaqProcess.Text = "Stop DAQ Process";  
                daqSystem.InitialisePorts();
                tmr.Start();
            }
            daqSystem.isActive = !daqSystem.isActive;
        }

        private void tbDebug_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
