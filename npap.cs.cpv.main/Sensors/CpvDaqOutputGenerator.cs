﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace npap.cs.cpv.main.Sensors
{
    public class CpvDaqOutputGenerator: npap.cs.cpv.CPVDaq, IOutputGenerator
    {
        public CpvDaqOutputGenerator(SerialPort TrackerPort):
            base(TrackerPort)
        {}

        StringBuilder sbHeaders = new StringBuilder();
        StringBuilder sbData = new StringBuilder();

        public string Headers(int no)
        {
            sbHeaders.Clear();
            sbHeaders.AppendFormat("CPV.TimeStamp;" );
            sbHeaders.AppendFormat("UTC.TimeStamp;" );
            sbHeaders.AppendFormat("IsValid;");
            sbHeaders.AppendFormat("TimeZone;" );
            sbHeaders.AppendFormat("TrackingMode;" );
            sbHeaders.AppendFormat("As.Azimuth;" );
            sbHeaders.AppendFormat("As.Elevation;" );            
            sbHeaders.AppendFormat("En.Azimuth;" );
            sbHeaders.AppendFormat("En.Elevation;" );
            sbHeaders.AppendFormat("StatusID;" );
            sbHeaders.AppendFormat("ISS.X;" );
            sbHeaders.AppendFormat("ISS.Y;" );
            sbHeaders.AppendFormat("ISS.Rad;" );
            sbHeaders.AppendFormat("WindSpeed;" );
            sbHeaders.AppendFormat("ErrorCode;" );
            sbHeaders.AppendFormat("Inputs1;" );
            sbHeaders.AppendFormat("Inputs2;" );
            sbHeaders.AppendFormat("NextTrackingTime;" );
            return sbHeaders.ToString();
        }

        public string DataString()
        {
            sbData.Clear();
            sbData.AppendFormat("{0};", this.CPVData.CPVTimestamp.ToString("yyyyMMddHHmmss"));
            sbData.AppendFormat("{0};", this.CPVData.UTCTimestamp.ToString("yyyyMMddHHmmss"));
            sbData.AppendFormat("{0};", this.CPVData.IsValidPacket);
            sbData.AppendFormat("{0};", this.CPVData.TimeZone);
            sbData.AppendFormat("{0};", this.CPVData.TrackingMode);
            sbData.AppendFormat("{0};", this.CPVData.AstronomicalPosition.X);
            sbData.AppendFormat("{0};", this.CPVData.AstronomicalPosition.Y);
            sbData.AppendFormat("{0};", this.CPVData.EncoderPosition.X);
            sbData.AppendFormat("{0};", this.CPVData.EncoderPosition.Y);
            sbData.AppendFormat("{0};", this.CPVData.StatusID);
            sbData.AppendFormat("{0};", this.CPVData.SunDeviationISS.X);
            sbData.AppendFormat("{0};", this.CPVData.SunDeviationISS.Y);
            sbData.AppendFormat("{0};", this.CPVData.ISSRadiation);
            sbData.AppendFormat("{0};", this.CPVData.WindSpeed);
            sbData.AppendFormat("{0};", this.CPVData.ErrorCode);
            sbData.AppendFormat("{0};", this.CPVData.Inputs1);
            sbData.AppendFormat("{0};", this.CPVData.Inputs2);
            sbData.AppendFormat("{0};", this.CPVData.NextTrackingTime);
            return sbData.ToString();
        }
        }
    }
