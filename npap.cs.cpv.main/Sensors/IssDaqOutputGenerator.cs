﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace npap.cs.cpv.main.Sensors
{
    public class IssDaqOutputGenerator: npap.cs.iss.ISSComms, IOutputGenerator
    {
        StringBuilder sbHeaders = new StringBuilder();
        StringBuilder sbData = new StringBuilder();

        public IssDaqOutputGenerator(string TrackerPort) :
            base(TrackerPort)
        {}

        public string Headers(int no)
        {
            sbHeaders.AppendFormat("Accepted{0};",no);
            sbHeaders.AppendFormat("SensorID{0};", no);
            sbHeaders.AppendFormat("SunX{0};", no);
            sbHeaders.AppendFormat("SunY{0};", no);
            sbHeaders.AppendFormat("Radiation{0};", no);
            sbHeaders.AppendFormat("Temp{0};", no);
            sbHeaders.AppendFormat("AddInfo{0};", no);
            return sbHeaders.ToString();
        }

        public string DataString()
        {
            sbData.Clear();
            sbData.AppendFormat("{0};", this.issData.Accepted);
            sbData.AppendFormat("{0};", this.issData.SensorID);
            sbData.AppendFormat("{0};", this.issData.X);
            sbData.AppendFormat("{0};", this.issData.Y);
            sbData.AppendFormat("{0};", this.issData.Radiation);
            sbData.AppendFormat("{0};", this.issData.Temp);
            sbData.AppendFormat("{0};", this.issData.AddInfo );
            return sbData.ToString();
        }
    }
}
