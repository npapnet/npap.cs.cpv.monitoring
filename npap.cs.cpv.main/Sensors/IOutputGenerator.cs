﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.cs.cpv.main.Sensors
{
    interface IOutputGenerator
    {
        string Headers(int no);
        string DataString();
    }
}
