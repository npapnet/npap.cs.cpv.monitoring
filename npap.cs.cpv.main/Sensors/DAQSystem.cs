﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using npap.cs.cpv;
using npap.cs.iss;

namespace npap.cs.cpv.main.Sensors
{
    public class DAQSystem
    {
        public bool isActive { get; set; }
        SerialPort TrackerPort = new SerialPort("COM11", 9600, Parity.None, 8, StopBits.One);  // Tracker port
       
        public CpvDaqOutputGenerator cpv { get; set; }
        public IssDaqOutputGenerator iss1 { get; set; }
        public IssDaqOutputGenerator iss2 { get; set; }
        public IssDaqOutputGenerator iss3 { get; set; }

        public DAQSystem()
        {
            isActive = false;
            cpv = new CpvDaqOutputGenerator(TrackerPort);
            iss1 = new IssDaqOutputGenerator("COM12");
            iss2 = new IssDaqOutputGenerator("COM13");
            iss3 = new IssDaqOutputGenerator("COM14");

        }
        #region Initialise and Close Serial Ports

        public void InitialisePorts()
        {
            cpv.InitSerialPort();
            iss1.InitSerialPort();
            iss2.InitSerialPort();
            iss3.InitSerialPort();
        }
        public void ClosePorts()
        {
            if (TrackerPort.IsOpen)
            { 
                TrackerPort.Close();
            }
            iss1.CloseSerialPort();
            iss2.CloseSerialPort();
            iss3.CloseSerialPort();
        }
        #endregion


        internal void PerformMeasurement()
        {
            try
            {
                Task<CpvData> taskCPV = Task<CpvData>.Factory.StartNew(() => cpv.PerformMeasurement());
                Task<ISSData> taskIss1 = Task<ISSData>.Factory.StartNew(() => PerformIssMeasurement(iss1));
                Task<ISSData> taskIss2 = Task<ISSData>.Factory.StartNew(() => PerformIssMeasurement(iss2));
                Task<ISSData> taskIss3 = Task<ISSData>.Factory.StartNew(() => PerformIssMeasurement(iss3));

                taskIss1.Wait();
                taskIss2.Wait();
                taskIss3.Wait();
                taskCPV.Wait();
            }
            catch
            {

            }
        }

        private ISSData PerformIssMeasurement(ISSComms issSensor)
        {
            issSensor.SendRequest();
            Thread.Sleep(200);
            issSensor.ReadFromPort();
            return issSensor.issData;
        }
    }
}
