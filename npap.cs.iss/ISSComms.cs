﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;

namespace npap.cs.iss
{
    public class ISSComms
    {
        #region Variable Declaration
        public int DataAtSerialPort { get; set; }
        public ISSData issData { get; set; }
        public string DataAsHexString { get; set; }
        public int discardedDataCounter;
        public bool SerialPortInitialised { get; set; }
        public DateTime StartMeasurement { get; set; }

        private SerialPort TPort;
        private ISSParser _ISSParser;
        private bool sendingRequest = false;
        private string _ComString;
        
        #endregion


        public ISSComms(string COMString)
        {
            this._ComString = COMString;
            this.issData = new ISSData(); 
            _ISSParser=new ISSParser();
            discardedDataCounter=0;
            SerialPortInitialised = false;
        }


        /// <summary>
        /// Function to convert minutes to ms
        /// </summary>
        /// <param name="minute"></param>
        /// <returns></returns>
        int CalculateTimerInterval(int minute)
        {
            if (minute <= 0)
                minute = 60;
            DateTime now = DateTime.Now;

            DateTime future = now.AddMinutes((minute - (now.Minute % minute))).AddSeconds(now.Second * -1).AddMilliseconds(now.Millisecond * -1);

            TimeSpan interval = future - now;

            return (int)interval.TotalMilliseconds;
        }


        #region ISSData Functions
        /// <summary>
        /// Converts ISS STring Packet to Hex String
        /// </summary>
        /// <param name="_issDataPacket"></param>
        /// <returns></returns>
        private string ISSDataPacketToHexString(string _issDataPacket)
        {
            byte[] byteArr = _ISSParser.getByteArray(_issDataPacket);

            return ISSDataPacketToHexString(byteArr);
        }

        /// <summary>
        /// ISS Packet (byte[]) to hex string Basic
        /// </summary>
        /// <param name="byteArr"></param>
        /// <returns></returns>
        private string ISSDataPacketToHexString(byte[] byteArr)
        {

            StringBuilder sb = new StringBuilder(byteArr.Length * 2);
            foreach (byte element in byteArr)
            {
                sb.AppendFormat("{0:x2} ", element);
            }
            return sb.ToString();
        }

        #endregion

        #region Serial Port Read
        /// <summary>
        /// Initialises Com Port
        /// </summary>
        /// <param name="_ComString">string</param>
        public void InitSerialPort()
        {
            try
            {

#if(VERBOSE)
                Console.WriteLine("Trying to open Com Port ");
#endif
                TPort = new SerialPort(_ComString, 115200, Parity.None, 8, StopBits.One);  // ISSport
                TPort.Open();

                SerialPortInitialised = true;
                StartMeasurement = DateTime.Now;
#if(VERBOSE)
                Console.WriteLine("Com Port {0} Initialised",_ComString);
#endif
            }
            catch
            {
                Console.WriteLine("Could not open Serial port!");
            }

        }

        /// <summary>
        /// Event which sends the request .
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SendRequest()
        {
            //t.Interval = CalculateTimerInterval(CHECK_INTERVAL);
            try
            {
                if (TPort.IsOpen)
                { // Proceed only if Tport is open.
                    Thread sendReqThread = new Thread(new ThreadStart(() =>
                    {
                        // the sending request flag signals that there is  writting in the process 
                        sendingRequest = true;
                        /*TODO short code for activating chkBxSendingdata
                        chkBxSendingData.BeginInvoke(
                            new Action(() => { chkBxSendingData.Checked = sendingRequest; })
                            );*/
                        try
                        {
                            byte[] SendRequest = { 0x11, 0x02, 0x63, 0x63 };
                            TPort.Write(SendRequest, 0, 4);
                        }
                        catch (Exception)
                        {

                        }
                        sendingRequest = false;
                    }));
                    if (!sendingRequest)
                    {
                        sendReqThread.Name = "Send Request thread.";
                        sendReqThread.Start();
                    }


                }

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Function that get done when  Timer is activated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ReadFromPort()
        {
            byte[] byteArr = new byte[20];
            int i = 0;
            //t.Interval = CalculateTimerInterval(CHECK_INTERVAL);
            try
            {
                if (TPort.IsOpen)
                { // Proceed only if Tport is open.

                    if (TPort.BytesToRead == 16)
                    {
                        DataAtSerialPort = TPort.BytesToRead;


                        for (i = 0; i < 16; i++)
                        {
                            byteArr[i] = (byte)TPort.ReadByte();
                        }

                        DataAsHexString = ISSDataPacketToHexString(byteArr);

                        this.issData = _ISSParser.parseByteArray(byteArr);
                        if (!this.issData.Accepted)
                        {// assumes that if an incorrect packet is received then the data flow is not in sync. Theerefore data should be flushed
                            TPort.DiscardInBuffer();
                        }

                    }
                    else if (TPort.BytesToRead == 20)
                    {   // process data if data are pushed from other 
                        byte[] b20Arr = new byte[20];
                        for (i = 0; i < 20; i++)
                        {
                            b20Arr[i] = (byte)TPort.ReadByte();
                        }

                        int st = -1;
                        switch ((byte)(b20Arr[0] / 16))
                        {
                            case 6:
                                st = 0;
                                break;
                            case 1:
                                st = 4;
                                break;
                            default:
                                break;
                        }

                        for (i = 0; i < 16; i++)
                        {
                            byteArr[i] = b20Arr[i + st];
                        }


                        DataAsHexString = ISSDataPacketToHexString(byteArr);

                        if (!this.issData.Accepted)
                        {// assumes that if an incorrect packet is received then the data flow is not in sync. Theerefore data should be flushed
                            TPort.DiscardInBuffer();
                        }
                    }
                    else
                    {

                        discardedDataCounter += 1;
                        Console.WriteLine("Data {0} Discarded from buffer. {1} bytes at port", discardedDataCounter, TPort.BytesToRead);
                        Console.WriteLine("Incoming Data:");
                        string str = TPort.ReadExisting();
                        DataAsHexString = str + ":" + ISSDataPacketToHexString(str);
                        Console.WriteLine(ISSDataPacketToHexString(str));
                        //TPort.DiscardInBuffer();

                    }
                }
            }
            catch (Exception)
            {

            }
        }
        /// <summary>
        /// Closes Serial Port and Enables again Init button and Combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CloseSerialPort()
        {
            try
            {
                sendingRequest = false;
                Console.WriteLine("Attempting to Close {0}!", TPort.PortName);
                TPort.DiscardInBuffer();
                TPort.Close();
                Console.WriteLine("Tracker Com Port is now Closed!");
                SerialPortInitialised =false;

            }
            catch
            {
                Console.WriteLine("Could not close Tracker Com Port");
                SerialPortInitialised = false;
            }
        }

/*
        /// <summary>
        /// Event which handles saving to file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tick_SaveToFile(object sender, EventArgs e)
        {
            try
            {
                if (_saveToFile && file.BaseStream != null)
                {
                    string str = Xcnt.ToString() + "\t" + String.Format("{0:yyyy/MM/dd}\t{1:HH:mm:ss.ff}", DateTime.Now, DateTime.Now) + "\t" + issControl.getString();//"" + k.ToString() + "" + (k * k).ToString();
                    file.WriteLine(str);
                    Console.WriteLine(str);
                }
            }
            catch (Exception)
            {
            }
        }
        */

        #endregion
    }
}
