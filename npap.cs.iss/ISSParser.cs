﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.cs.iss
{
    /// <summary>
    /// Class for parsing data.
    /// </summary>
    internal class ISSParser
    {
        private ISSData _myISS;

        public enum ISSCommands
        {
            Request = 1,
            Config = 2,
            BitRate = 3,
            Temperature = 4,
            ConfigAck = 5,
            Angles = 6
        };

        public ISSParser()
        {

        }

        /// <summary>
        /// Parset a data packet
        /// calls parseByteArray(string)
        /// </summary>
        /// <param name="dataPacket">String </param>
        /// <returns></returns>
        public ISSData parsePacket(string dataPacket)
        {

            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] array = System.Text.Encoding.ASCII.GetBytes(dataPacket);
            foreach (byte element in array)
            {
                //Console.WriteLine("{0} = {1}", element, (char)element);
                Console.Write("{0} ", element);
            }
            Console.WriteLine();

            _myISS = parseByteArray(array);
            return _myISS;
        }


        /// <summary>
        /// get byte array from data packet
        /// </summary>
        /// <param name="dataPacket">String </param>
        /// <returns></returns>
        public byte[] getByteArray(string dataPacket)
        {

            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] array = System.Text.Encoding.ASCII.GetBytes(dataPacket);
            return array;
        }



        /// <summary>
        /// Parse Data Byte Array - actual implementation
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public ISSData parseByteArray(byte[] array)
        {
            byte expSensorID=(byte)(array[0] % 16);
            ISSData tempISS = new ISSData();
            byte DTS;
            //Get Data type and command
            byte DataType = (byte)(array[0] / 16);
            tempISS.SensorID = (byte)(array[0] % 16);
#if (ISSDEBUG)
            Console.WriteLine("Command Type = {0}, SensorID,{1}", DataType, tempISS.SensorID);       
#endif

            switch (DataType)
            {
                case (byte)ISSCommands.Request:  // request
                    DTS = 0x02;
                    break;
                case 2:  // config
                    DTS = 0x02;
                    break;
                case 3: // Set Bitrate
                    DTS = 0x02;
                    break;
                case (byte)ISSCommands.Temperature: //Temperature
                    DTS = 0x0E;
                    break;
                case (byte)ISSCommands.ConfigAck:  // Config Acknowledge
                    DTS = 0x02;
                    break;
                case (byte)ISSCommands.Angles:  //Angles
                    DTS = 0x0E;
                    break;
                default:
                    DTS = 0x00;
                    break;
            }


            if (array[1] == DTS && DataType == (byte)ISSCommands.Angles && (expSensorID == tempISS.SensorID))
            {
                tempISS.X = System.BitConverter.ToSingle(array, 2);
                tempISS.Y = System.BitConverter.ToSingle(array, 6);
                tempISS.Radiation = System.BitConverter.ToSingle(array, 10);
                tempISS.AddInfo = array[14];
                int i = 0;
                byte CRCsum = 0;
                for (i = 0; i < 13; i++)
                {
                    CRCsum += array[2 + i];
                }
#if(ISSDEBUG)
               Console.WriteLine("X:{0}, Y:{1},Rad{2}", tempISS.X, tempISS.Y, tempISS.Radiation);
               Console.WriteLine("Addinfo:{0}, CRC:{1},CRC:{2}", tempISS.AddInfo, array[15], CRCsum);
#endif
                tempISS.Accepted = true;
            }
            else
            {  //Ignore packet
                Console.WriteLine("Packet Has been ignored");
                tempISS.Accepted = false;
            }


            return tempISS;
        }

        #region Static Bytes

        /// <summary>
        /// http://stackoverflow.com/questions/472906/net-string-to-byte-array-c-sharp
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private byte[] mGetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        #endregion

    }

}
