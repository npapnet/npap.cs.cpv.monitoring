﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.cs.iss
{
    /// <summary>
    /// Structure that stores the ISS data
    /// </summary>
    public class ISSData
    {
        public bool Accepted { get; set; }
        public int SensorID { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Radiation { get; set; }
        public double Temp { get; set; }
        public int AddInfo { get; set; }

        public ISSData()
        {
            this.Accepted = false;
            this.SensorID = 1;
            this.X = double.NaN;
            this.Y = double.NaN;
            this.Radiation = double.NaN;
            this.Temp = double.NaN;
            this.AddInfo = -1;
        }

    }
}
