﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.cs.cpv
{

    public class CpvData
    {
        private static DateTime dtOrigin = new DateTime(1980, 1, 1);
        public int TrackerID { get; set; }
        public DateTime CPVTimestamp { get; set; }
        public DateTime UTCTimestamp { get; set; }
        public byte TimeZone { get; set; }
        public byte TrackingMode{ get; set; }
        public PositionData<double> AstronomicalPosition { get; set; }
        public PositionData<int> EncoderPosition { get; set; }
        public byte StatusID { get; set; }
        public PositionData<double> SunDeviationISS { get; set; }
        public double ISSRadiation { get; set; }
        public double WindSpeed { get; set; }
        public byte ErrorCode { get; set; }
        public byte Inputs1 { get; set; }
        public byte Inputs2 { get; set; }
        public DateTime NextTrackingTime { get; set; }
        public bool IsValidPacket { get; set; }

        public CpvData()
        {
            this.AstronomicalPosition = new PositionData<double>();
            this.EncoderPosition = new PositionData<int>();
            this.SunDeviationISS = new PositionData<double>();
            this.ErrorCode = 255;
            this.Inputs1 = 255;
            this.Inputs2 = 255;
            this.NextTrackingTime = dtOrigin;

        }
    }
}
