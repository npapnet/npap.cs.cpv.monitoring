﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;

namespace npap.cs.cpv
{
    public class CpvComms
    {

        private string _CommandString = "#,48,48,48,49,100,204,4\r";
        private SerialPort _TrackerPort;
        public string testDataParallelism { get; set; }

        public CpvComms(SerialPort trackerPort)
        {
            this._TrackerPort = trackerPort;
        }


        #region Pre and Post methods
        private void InitialisePort()
        {
            if (!_TrackerPort.IsOpen)
            {
                _TrackerPort.Open();
            }
        }

        private void PostOperation()
        {
            if (_TrackerPort.IsOpen)
            {
                _TrackerPort.Close();
            }
        }
        #endregion

        public string TrackerPortGetData()
        {
            if (_TrackerPort.IsOpen)
            { 
                //TrackerPort.DataReceived += TrackerPort_DataReceived;
                //_TrackerPort.Open();
                _TrackerPort.DiscardInBuffer();
                _TrackerPort.Write(_CommandString);
            }
            Thread.Sleep(500);
            string strout = "";
            try
            {
                strout = ReadFromPort();
            }
            catch (Exception)
            {

            }
            //_TrackerPort.DiscardInBuffer();
            testDataParallelism = strout;
            return strout;
        }

        private string ReadFromPort()
        { 
            string str = "";
            try
            {
                if (_TrackerPort.IsOpen)
                { // Proceed only if Tport is open.

                    str = _TrackerPort.ReadExisting();

                    _TrackerPort.DiscardInBuffer();
                }

            }
            catch (Exception)
            {

            }
            return str;
        }

        internal void InitSerialPort()
        {
            try
            {
                _TrackerPort.Open();
            }
            catch (IOException)
            { }
        }

        internal void CloseSerialPort()
        {
            _TrackerPort.Close();
        }
    }
}
