﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.cs.cpv
{
    public class PositionData<T>
    {
        public T X { get; set; }
        public T Y { get; set; }
    }
}
