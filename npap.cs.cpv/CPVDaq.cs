﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace npap.cs.cpv
{
    public class CPVDaq
    {
        CpvComms cpvComms;
        private CpvDataParser cdp = new CpvDataParser();
        public CpvData CPVData { get; set; }
        string strout="";

        public CPVDaq(SerialPort TrackerPort)
        {
            cpvComms= new CpvComms(TrackerPort);
        }

        public CpvData PerformMeasurement ()
        {
            //Initialisation
            strout ="";
            strout= cpvComms.TrackerPortGetData();
            this.CPVData = cdp.ParseData(strout);
            return CPVData;
        }

        public void InitSerialPort()
        {
            cpvComms.InitSerialPort();

        }
        internal void CloseSerialPort()
        {
            cpvComms.CloseSerialPort();
        }
    }
}
