﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace npap.cs.cpv
{
    public class CpvDataParser
    {
        public CpvData CpvStatus { get; set; }
        public static DateTime dtReferenceOrigin = new DateTime(1980, 1, 1);
        
        public CpvDataParser()
        {
            CpvStatus = new CpvData();
            
        }

        public CpvData ParseData(string dataResponse)
        {

            string[] data = dataResponse.Trim().Split(new char[] { ',', '#', '!', '\r', '?' }, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length != 53 || !(dataResponse.Substring(0, 2).Contains('!')))
            {
                //throw new Exception("Invalid Packet");
                CpvStatus.IsValidPacket = false;
                return this.CpvStatus;
                //System.Windows.Forms.MessageBox.Show(dataResponse + "\n" + data.Length.ToString());
            }
            //CpvStatus.CPVTimestamp = ;
            CpvStatus.IsValidPacket = true;
            List<byte> dataList = new List<byte>();
            foreach (string str in data)
            {
                dataList.Add( Convert.ToByte(str));
            }
            byte[] databytes = dataList.ToArray();
            //TODO: need to properly address this. 
            // One should convert ASCII (48) ->'0', then assemble the string '0001' and convert the string to int
            CpvStatus.TrackerID = 1;
            
            //CPVTimestamp
            CpvStatus.CPVTimestamp = dtReferenceOrigin.AddSeconds(BitConverter.ToInt32(databytes.ToArray(), 5));
            //UTCTimestamp
            CpvStatus.UTCTimestamp = DateTime.UtcNow;
            CpvStatus.TimeZone = databytes[9];
            CpvStatus.TrackingMode= databytes[10];
            //Astronomical Position
            CpvStatus.AstronomicalPosition.X = BitConverter.ToSingle(databytes.ToArray(), 11);
            CpvStatus.AstronomicalPosition.Y = BitConverter.ToSingle(databytes.ToArray(), 15);

            //Encoder
            CpvStatus.EncoderPosition.X = BitConverter.ToInt32(databytes.ToArray(), 19);
            CpvStatus.EncoderPosition.Y = BitConverter.ToInt32(databytes.ToArray(), 23);
            //System.Windows.Forms.MessageBox.Show(CpvStatus.EncoderPosition.X.ToString() + "\n" + CpvStatus.EncoderPosition.Y.ToString());
            
            CpvStatus.StatusID = databytes[27];

            //Astronomical Position
            CpvStatus.SunDeviationISS.X = BitConverter.ToSingle(databytes.ToArray(), 28);
            CpvStatus.SunDeviationISS.Y = BitConverter.ToSingle(databytes.ToArray(), 32);
            CpvStatus.ISSRadiation = BitConverter.ToSingle(databytes.ToArray(), 36);
            CpvStatus.WindSpeed = BitConverter.ToSingle(databytes.ToArray(), 40);
            CpvStatus.ErrorCode = databytes[44];
            CpvStatus.Inputs1= databytes[45];
            CpvStatus.Inputs2 = databytes[46];
            CpvStatus.NextTrackingTime=  dtReferenceOrigin.AddSeconds(BitConverter.ToInt32(databytes.ToArray(), 47));
            //throw new NotImplementedException();
            return this.CpvStatus;
        }

        
    }
}
